# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 09:35:41 2019

diffusion subtraction main methods

@author: yufengh
"""

import numpy as np
from scipy.signal import convolve

def generate_kernel(D, dt, roi = (101, 101)):
    
    ft0 = 1
    y, x = np.mgrid[0:roi[0], 0:roi[1]]
    
    x0 = roi[1]//2
    y0 = roi[0]//2
    
    exponent = -((x-x0)**2 + (y-y0)**2)/(4*D*dt)
    front = ft0/(4*np.pi*D*dt)
    kernel = front*np.exp(exponent)
    
    return kernel

def diffusion_calc (img, D = 480, t = 0.002, pad = 50):
    
    kernel = generate_kernel(D, t)
    imgpad = np.pad(img, pad, mode = 'reflect')
    out = convolve(imgpad, kernel, mode = 'same')
    out = out[pad:-pad, pad:-pad]
    return out

def t1_subtraction (img, tl = 2, D = 480, t = 0.002, offset = 0):
    
    out = np.empty(img.shape)
    
    for i in range(len(img)):
        if i <= tl:
            out[i] = img[i]
        
        else:
            out[i] = img[i] - diffusion_calc(img[i-tl], D, t*tl)
            
        if i%100 == 0:
            print ('step {0} of {1}'.format(i, len(img)))
            
    out = out.astype(np.float32)
    return out[tl+1:]