# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 12:18:03 2020

@author: Yufengh
"""

import numpy as np
import matplotlib.pyplot as plt
from skimage import io
import scipy.ndimage as nd
import pandas as pd
from skimage import filters
import thresholding
import os

def generate_mask_local(img, area = 101, offset = -0.00005):
    mask = (img > filters.threshold_local(img, area, offset = offset)).astype(np.int32)
    return mask
def generate_mask_otsu(img):
    mask = (img > filters.threshold_otsu(img, nbins = 2048)).astype(np.int32)
    return mask

def measure_clusters(dir, outdir = None, mskdir = None):
    sizes = []
    for fname in os.listdir(dir):
        if fname.endswith('.tif'):
            img = io.imread(dir + fname)
            fref = fname.split('.')[0]
            if mskdir:
                fref = fname.split('_ryr')[0]
                msk = io.imread(mskdir + fref + '_ca_cl_mask.tif')
                msk = nd.zoom(msk, 13)
                msk[msk > 0] = 1
                crop = [min([len(msk), len(img)]), min([len(msk[0]), len(img[0])])]
                msk = msk[5:crop[0]-6, 5:crop[1]-6]
                
                img = img[5:crop[0]-6, 5:crop[1]-6]
                
            else:
                img = img[50:-51, 50:-51]
                
            #sf = thresholding.signalFraction(img, 0.5)
            img = generate_mask_local(img, area = 101, offset = -300)
            #img = generate_mask_otsu(img)
            #img = (img > sf).astype(int)

            if mskdir:
                filt = msk*img
            else:
                filt = img
            #plt.imshow(filt)
            lab, nobs = nd.label(filt)
            tsizes = nd.sum(filt, labels = lab, index = np.arange(1, nobs))
            label = dir.split('\\')[-3]+'_'+fname
            cell = np.repeat(label, len(tsizes))
            imsize = np.repeat(len(img)*len(img[0]), len(tsizes))
            data = {'size':tsizes, 'sref':cell, 'imarea':imsize}
            df = pd.DataFrame(data)
            
            if outdir:
                df.to_csv(outdir + fref + '_{}'.format(label) + '_sizes.csv')
                io.imsave(outdir + fref + '_{}'.format(label) + '_segment.tif', filt.astype(np.uint8))
                
            sizes.append(df)
    sizesdf = pd.concat(sizes)
    #sizes = sizes.flatten()
    if outdir:
        sizesdf.to_csv(outdir+'_{}'.format(label) + 'total_sizes.csv') 
    return sizes

if __name__ == '__main__':
    #dir = 'E:\\Dstorm Surface\\m3_sec_ctl\\render\\'
    #outdir = 'E:\\Dstorm Surface\\output2\\m3_sec\\'
    
    dir = 'E:\\Dstorm Surface\\m1\\render2\\'
    outdir = 'E:\\Dstorm Surface\\output2\\m1\\'
    
    #dir = 'F:\\Correlation_paper\\sr\\ryr\\render\\'
    #outdir = 'F:\\Dstorm Surface\\render\\output_PALM\\'
    mskdir = 'E:\\Correlation_paper\\mask\\'