# -*- coding: utf-8 -*-
"""
Created on Wed Oct  2 12:53:50 2019

interpolation for background estimation and removal

please refer to methods in article for method

@author: yufengh
"""

import numpy as np
from skimage import io
import scipy.ndimage as nd
import matplotlib.pyplot as plt
from scipy.ndimage.morphology import binary_dilation, binary_erosion
from scipy import interpolate
import time
import tqdm
import os


def removal(img, o1_offset = 3, o2_offset = 1):
    
    """
    mask for pixels in sequence which show non background levels of signal
    img - image seq
    o1_offset - offset from baseline for significant detection (1st derivative)
    o2_offset - offset from baseline for significant detection (2nd derivative)
    """
    
    img = img.astype(np.float32)

    derivative_o1 = np.abs(nd.gaussian_filter1d(img, sigma = 10, order = 1, axis = 0))
    derivative_o1 = np.nan_to_num(derivative_o1)
    derivative_o2 = np.abs(nd.gaussian_filter1d(img, sigma = 10, order = 2, axis = 0))
    derivative_o2 = np.nan_to_num(derivative_o2)

    removal_map = np.logical_or(derivative_o1 > o1_offset,
                                derivative_o2 > o2_offset)

    removal_map = binary_erosion(removal_map, iterations = 5)
    removal_map = binary_dilation(removal_map, iterations = 50)
    print (removal_map.shape)

    return removal_map

def interpolate_bg(xpos, ypos, removal, pixel_ref, t_range):
    
    """
    interpolation for specific pixel in image over time
    """

    xposfilt = xpos[removal == False]
    yposfilt = ypos[removal == False]

    knots = np.linspace(xpos.min(), xpos.max(), 7, dtype = int)[1:-2]
    """spline interp does not require the X[0] or x[-1] knots"""

    interpolation = interpolate.LSQUnivariateSpline(xposfilt, yposfilt, knots)
    xseq = np.arange(t_range[0], t_range[1])
    ypred = interpolation(xseq)


    return ypred, pixel_ref

def process_img(img, start = 0):
    img = img[start:]
    bg = np.zeros(img.shape)
    removemap = removal(img)
    posx, posy = np.mgrid[0:img.shape[1], 0:img.shape[2]]
    posx = posx.flatten()
    posy = posy.flatten()

    for i in tqdm.tqdm(range(len(posx))):
        #print ("current pos {0},{1}: {2} of {3}".format(posx[i], posy[i], i, len(posx)))
        seq = img[:, posx[i], posy[i]]
        seq = np.nan_to_num(seq)
        seq, ref = interpolate_bg(np.arange(len(seq)), seq,
                             removemap[:, posx[i], posy[i]],
                             [posx[i],posy[i]],
                             (0, len(seq)))
        bg[:, posx[i], posy[i]] = seq

    output = img - bg
    return output, bg


def median(seq):
    
    """
    function for calculating medians, used for comparison on speed
    """

    t0 = time.time()
    med1 = nd.median_filter(seq, 400)
    t1 = time.time()
    print ('funct time: {}s'.format(t1-t0))

    return med1

def batch(dir, outdir):
    
    """
    batch processing
    """

    for filename in os.listdir(dir):
        if filename.endswith('.tif'):
            fname = filename.split('.')[0]
            img = io.imread(dir + filename)
            out, bg = process_img(img, start = 0)
            io.imsave(outdir + fname + '_bg.tif', bg.astype(np.float32))
            io.imsave(outdir + fname + '_cl.tif', out.astype(np.float32))

    return

if __name__ == '__main__':
	indir = 'test\\'
	outdir = 'test\\raw\\'
	batch(indir, outdir)
