# -*- coding: utf-8 -*-
"""
Created on Tue Nov 06 15:35:33 2018
resizer
@author: yufengh
"""
import numpy as np



def resize(img, spacing):
    out = []
    zlen = len(img)
    scaled_size = zlen//spacing
    if zlen%spacing != 0:
        
        new_zlen = scaled_size*spacing
        img = img[:new_zlen].copy()
    
    fragments = np.array_split(img, scaled_size, axis = 0)
    
    for i in range(len(fragments)):
        sliceave = np.mean(fragments[i],axis = 0)
        out.append(sliceave)
        
    out = np.array(out)
    return img, out

def resize_xy_2d(img):
    ind1 = img[::2,::2]
    ind2 = img[1::2,::2]
    ind3 = img[::2,1::2]
    ind4 = img[1::2,1::2]
    return (ind1+ind2+ind3+ind4)/4

def resize_xy_3d(img):
    ind1 = img[:,::2,::2]
    ind2 = img[:,1::2,::2]
    ind3 = img[:,::2,1::2]
    ind4 = img[:,1::2,1::2]
    return (ind1+ind2+ind3+ind4)/4