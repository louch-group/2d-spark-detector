# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 11:03:02 2019

!!! I'VE TEMPORARILY REMOVED THE LOC ACC CIRCLES !!!

Correlate data from spark measure to RyR mask

guide on using data:

loaddata-> input corrections for img -> create map -> create mask -> find neighbours -> save

modified 8-4-19
modified to make mapping sites better
the code needs a fair bit of reformatting to be more readable and comments added

modified 23-5-19
modified added new function for generating mask

@author: yufengh
"""

import numpy as np
from skimage import io, morphology
from matplotlib.collections import LineCollection, PatchCollection
import skimage.measure as ms
from sklearn.neighbors import NearestNeighbors
from skimage import filters
import matplotlib.pyplot as plt
import scipy.ndimage as nd
import pandas as pd
import os
from os import path



def gaussian_int(amplitude, sigmax, sigmay, scale = 1):

    return 2*np.pi*amplitude*sigmax*sigmay*scale**2

def gaussian(x, amp, width):
    """Returns a gaussian function with the given parameters"""

    width = float(width)
    a = 1/(2*width**2)

    """follows gaussian general formula detailed in wikipedia"""
    g = amp*np.exp(-(a*(x)**2))

    return g

def chen_formula(amplitude):
    amplitude = amplitude+1
    Kd = 320#fmol/mm3
    Ca_rest = 100#fmol/mm3

    conc = (Kd*amplitude)/((Kd/Ca_rest+1)-amplitude)#fmol/mm3
    vol = 4.225e-13#mm3
    n = conc*vol
    return n

def coulombs_law(ions, valence = 2, frame_time = 2e-3):
    charge = 1.6e-7 #pA/e
    time = frame_time #2ms
    flux = (ions*valence*charge)/time
    return flux #pA

def calculate_fit_vol(df):
    """
    sum of ff0 form fit parameters

    units returned is ff0.nm^2
    """
    df['fitvol'] = gaussian_int(df['ff0'],  df['xwidth'], df['ywidth'])
    df['fitvol_ds'] = gaussian_int(df['ff0_ds'],  df['xwidth_ds'], df['ywidth_ds'])

    nvol = chen_formula(df['ff0_ds']) #fmol
    nion = 6.0221409e+8 * nvol

    df['nCa_est(fmol)'] = gaussian_int(nvol,  df['xwidth_ds'], df['ywidth_ds'])
    df['nCa_est(ions)'] = gaussian_int(nion,  df['xwidth_ds'], df['ywidth_ds'])
    df['release_flux(pA)'] = coulombs_law(df['nCa_est(ions)'])
    return

def extract_events(label, points, pxl = 5):

    pt = points.astype(int)

    """make a 0 array"""
    output = np.zeros(label.shape)

    for i in pt:

        output[i[1]//pxl, i[0]//pxl] += 1

    sizes = nd.sum(output, label, index=np.arange(1, label.max() + 1))

    return sizes, output

# def generate_mask(img, thresh = 0.3):
#     lvl = thresholding.signalFraction(img, thresh)
#     mask = (img > lvl).astype(np.int32)
#     return mask

def generate_mask_local(img, area = 101, offset = -0.00005):
    mask = (img > filters.threshold_local(img, area, offset = offset)).astype(np.int32)
    return mask

def in_df(df, dist, cluster_ref, pixels):

    df['dist from c'] = dist
    df['release_ref'] = cluster_ref
    df['pixels'] = pixels
    return df


def cluster_detection(img, msk, points, minevents = 2, minarea = 9, minratio = 0.5):
    label, objn = nd.label(msk)
    pts, out = extract_events(label, points, pxl = 5)


    for i in np.arange(objn)+1:
        if np.sum(out[label == i]) < minevents:
            msk[label == i] = 0
            continue

        elif np.sum(out[label == i])*36/np.sum(msk[label == i]) < minratio:
            msk[label == i] = 0
            continue

        else:
            continue

    label, objn = nd.label(msk)
    sizes = nd.sum(msk, label, index=np.arange(1, objn + 1))
    events = nd.sum(img, label, index=np.arange(1, objn + 1))*100
    pts =  nd.sum(out, label, index=np.arange(1, objn + 1))
    return label, objn, sizes, events, pts


def cluster_detection_sc(msk, minsize):
    label, objn = nd.label(msk)
    for i in np.arange(objn)+1:
        if np.sum(msk[label == i]) < minsize:
            msk[label == i] = 0
            continue
        else:
            continue
    msk_sc = nd.binary_dilation(msk, iterations = 5)
    label, objn = nd.label(msk_sc)
    label = label*msk
    sizes = nd.sum(msk, label, index=np.arange(1, objn + 1))

    return label, objn, sizes


def nearest(point, label):
    """point array with 0 as x and 1 as y"""
    label_ypos, label_xpos = np.where(label>0)
    coordinates = np.array([label_xpos, label_ypos]).T
    nbrs = NearestNeighbors(1)

    nbrs.fit(coordinates)
    dists, index = nbrs.kneighbors(point, return_distance=True)

    return dists, index, coordinates


def contributors(point, mask, label, scale, contrib_thresh = 2):
    """
    identifies the sum of the contributing pixels within the mask as well as list the contributing clusters

    contrib thresh - standard deviations before pixels no longer considered contributing
    scale - scale of precision in pixels
    """
    label_ypos, label_xpos = np.where(label>0)
    coordinates = np.array([label_xpos, label_ypos]).T
    nbrs = NearestNeighbors()

    nbrs.fit(coordinates)
    print('calculating neighbours')
    dists = []
    index = []
    for i in range(len(point)):
        dists_t, index_t = nbrs.radius_neighbors(X = point[i].reshape(1,-1), radius = scale[i]*contrib_thresh, return_distance=True)
        # print('index check:')
        # print(index_t)
        dists.append(dists_t[0])
        index.append(index_t[0])

    contrib_pixel = []
    total_pixel = []
    nndist = []
    print('fitting neighbours')

    for c, i in enumerate(dists):
        if len(i) == 0:
            contrib_pixel.append(0)
            total_pixel.append(gaussian_int(1, scale[c], scale[c], scale = 1))
            nndist.append(10000)

        else:
            contrib_pixel.append(np.sum(gaussian(i, amp = 1, width = scale[c])))
            total_pixel.append(gaussian_int(1, scale[c], scale[c], scale = 1))
            nndist.append(i.min())


    contrib_pixel = np.array(contrib_pixel)
    total_pixel = np.array(total_pixel)
    local_density = contrib_pixel/total_pixel
    nndist = np.array(nndist)

    return contrib_pixel, nndist, index, coordinates, local_density


def find_neighbours(df, msk, img, events, uncertainty = 200, ds = True):
    label, objno, sizes, mean_dens, pts = cluster_detection(img, msk, events, 2)

    scale = (df['xwidth_ds'].values*13+df['ywidth_ds'].values*13)/2
    if ds:
        pointx = df['truex_ds'].values
        pointy = df['truey_ds'].values

    else:
        pointx = df['truex'].values
        pointy = df['truey'].values

    points = np.array([pointx, pointy]).T
    pixels, dist, index, coordinates, local_density = contributors(points, msk, label, contrib_thresh = 2, scale = scale)

    cluster_ref = []
    if len(index) > 0:
        print('beginning neighbour association')
        for i in index:
            tempref = []
            for j in i:
                """x and y is reversed in index"""
                tempref.append(label[coordinates[j, 1], coordinates[j, 0]])

            try:
                cluster_ref.append(np.unique(tempref))

            except:
                print ('tempref not assigned')


    cluster_ref = np.array(cluster_ref, dtype = object)

    dist[dist <= 1] = 0

    return dist, cluster_ref, label, sizes, mean_dens, pts, pixels, local_density


def vector(center, target):
    return np.sqrt((center[0]-target[0])**2+(center[1]-target[1])**2)


"""calculate mean displacement from center of mass"""
def calculate_mean_displacement(df):

    xpos = df['xpos'].values
    ypos = df['ypos'].values

    center = (df['xpos'].mean(), df['ypos'].mean())

    deviation = np.empty(len(xpos))
    for i in range(len(xpos)):
        deviation[i] = vector(center, [xpos[i], ypos[i]])

    return np.mean(deviation)


def calculate_mean_displacement_set(df):

    sparkunique = df['sparkref'].unique()
    deviation_list = np.empty(len(sparkunique))

    for i in range (len(sparkunique)):
        sparkcourse = df.loc[df['sparkref'] == sparkunique[i]].copy()

        xpos = sparkcourse['xpos'].values
        ypos = sparkcourse['ypos'].values

        center = (df['xpos'].mean(), df['ypos'].mean())
        deviation = np.empty(len(xpos))
        for j in range(len(xpos)):
            deviation[j] = vector(center, [xpos[j], ypos[j]])

        deviation_list[i] = np.mean(deviation)

    return deviation_list


def calculate_centerd_pos(df):
    xpos = df['xpos'].values
    ypos = df['ypos'].values

    center = (df['xpos'].mean(), df['ypos'].mean())

    xcorrected = xpos - center[0]
    ycorrected = ypos - center[1]

    return xcorrected, ycorrected

#df = df[df['SNR'] > 1]
#my_dpi = 96
#df['truex'] = np.empty(len(df['xpos']))
#df['truey'] = np.empty(len(df['ypos']))
#df = df[:]
#
#reflist = spark_uniques.astype(str)
#fig, ax = plt.subplots(1, 1, figsize=(len(img[0])/my_dpi, len(img[1])/my_dpi), dpi = my_dpi)
#
#
#ax.matshow(img, cmap= plt.cm.inferno, vmin = -0.00005, vmax = 0.00025)


def filter_data(df, snr, margin, width):
    #df = df[df['SNR'] > snr]
    #df = df[df['yerr'] < margin]
    #df = df[df['xerr'] < margin]


    spark_uniques = df['sparkref'].unique()
    spark_filtered = pd.DataFrame()

    for i in range (len(spark_uniques)):
        sparkcourse = df.loc[df['sparkref'] == spark_uniques[i]].copy()
        sparkcourse = sparkcourse[1:]
        snrgroups, ngroups = nd.label(sparkcourse['SNR'].values > snr)
        print('groups:{}'.format(ngroups))
        max_cluster = 0
        if ngroups > 0:
            sizes = nd.sum(sparkcourse['SNR'].values > snr, snrgroups, np.arange(1, ngroups+1))
            print(sizes)
            max_cluster = sizes.max()

        if np.all(sparkcourse['xwidth'].values > width):
            continue
        if np.all(sparkcourse['ywidth'].values > width):
            continue
        if max_cluster < 4:
            continue
        else:
            spark_filtered = spark_filtered.append(sparkcourse)

    return spark_filtered

def calculate_index(df):

    sparkindex = df['sparkref'].unique()
    spark_index_list = np.empty(len(sparkindex))
    for i in range(len(sparkindex)):
        val = int(sparkindex[i].split("test")[-1])
        spark_index_list[i] = val

    return spark_index_list


def apply_offsets(vals, offset, scale):
    vals = vals*scale
    return vals + offset


def calculate_offsets(roi, sparkcourse, offx, offy, correctionx, correctiony,imgsizex, imgsizey, scale = 13):
    """roi reference is assuming equal size in x an y direction"""

    """offsets calculation before adding into function pixel offset followed by position offset"""
    offsetx = (offx)*scale + correctionx
    offsety = (offy)*scale + correctiony

    sparkcourse['true_xpos'] = apply_offsets(sparkcourse['xpos'].copy().values, offsetx, scale)
    sparkcourse['true_ypos'] = apply_offsets(sparkcourse['ypos'].copy().values, offsety, scale)

    sparkcourse['true_xpos_ds'] = apply_offsets(sparkcourse['xpos_ds'].copy().values, offsetx, scale)
    sparkcourse['true_ypos_ds'] = apply_offsets(sparkcourse['ypos_ds'].copy().values, offsety, scale)

    return sparkcourse


def create_map(df, df2, img, imgname, xcorr, ycorr, suppress_renders = False, outdir = None):

    df = filter_data(df, 4.5, 2, 12)
    if len(df) == 0:
        return pd.DataFrame(), pd.DataFrame(), pd.DataFrame(), pd.DataFrame()
    try:
        df['checkpoint_ds'].loc[df['xerr_ds'] > 1.0] = False
        df['checkpoint_ds'].loc[df['yerr_ds'] > 1.0] = False

        df['checkpoint_ds'].loc[df['xwidth_ds'] > 12] = False
        df['checkpoint_ds'].loc[df['ywidth_ds'] > 12] = False
        df['checkpoint_ds'].loc[df['xwidth_ds'] < 4.1] = False
        df['checkpoint_ds'].loc[df['ywidth_ds'] < 4.1] = False

        df['checkpoint_ds'].loc[df['SNR_ds'] < 3.0] = False

    except:
        print('issues with applying filters')
        return pd.DataFrame(), pd.DataFrame(), pd.DataFrame(), pd.DataFrame()


    my_dpi = 100
    df['truex'] = np.empty(len(df['xpos']))
    df['truey'] = np.empty(len(df['ypos']))
    df = df[:]
    df2 = df2.loc[calculate_index(df)]
    displacement = np.empty(len(df2))
    correctedx = np.array([])
    correctedy = np.array([])
    truex = np.array([])
    truey = np.array([])
    truex_ds = np.array([])
    truey_ds = np.array([])

    fig, ax = plt.subplots(1, 1, figsize=(len(img[0])/my_dpi, len(img[1])/my_dpi), dpi = my_dpi)
    ax.imshow(img, cmap = 'hot', vmin = 0, vmax = 0.002)
    spark_uniques = df['sparkref'].unique()
    for i in range (len(spark_uniques)):
        sparkcourse = df.loc[df['sparkref'] == spark_uniques[i]].copy()

        displacement[i] = calculate_mean_displacement(sparkcourse)
        center_correction = calculate_centerd_pos(sparkcourse)
        correctedx = np.append(correctedx, center_correction[0])
        correctedy = np.append(correctedy, center_correction[1])
        #a bunch of numbers pulled from the image
        sparkcourse = calculate_offsets(41, sparkcourse, df2['spark x'].values[i], df2['spark y'].values[i], xcorr, ycorr, 576, 288)
        truex = np.append(truex, sparkcourse['true_xpos'].values)
        truey = np.append(truey, sparkcourse['true_ypos'].values)
        truex_ds = np.append(truex_ds, sparkcourse['true_xpos_ds'].values)
        truey_ds = np.append(truey_ds, sparkcourse['true_ypos_ds'].values)


        sparkcourse['mean_err'] = (sparkcourse['xerr_ds'] + sparkcourse['yerr_ds'])/2

        sparkcourse_ds = sparkcourse[sparkcourse['checkpoint_ds'].values]

#        offsetx = df2['spark x'][i]
#        offsety = df2['spark y'][i]
#
#
#        """hacky code, i need to change this in the spark class to always ensure cropping doesnt
#        offset the centroid"""
#
#        """adjust for edge cropping"""
#
#        addoffsetx = 0
#        addoffsety = 0
#        if offsetx < 21:
#            addoffsetx = (21-offsetx)//2
#        if offsetx > 487:
#            addoffsetx = ((508 - offsetx)-21)//2
#        if offsety < 21:
#            addoffsety = (21-offsety)//2
#        if offsety > 103:
#            addoffsety = ((124 - offsety)-21)//2

        """converting to pixel vals"""
#        offset_array_x = (sparkcourse['xpos'].copy().values + offsetx + addoffsetx )*12.5
#        offset_array_y = (sparkcourse['ypos'].copy().values + offsety + addoffsety )*12.5
#        sparkcourse['truex'] = offset_array_x+(-34)+0
#        sparkcourse['truey'] = offset_array_y+(-68)+0
        #df.loc[df['sparkref'] == spark_uniques[i], 'truex'] = offset_array_x
        #df.loc[df['sparkref'] == spark_uniques[i], 'truey'] = offset_array_y


        xposbig = sparkcourse['true_xpos'].values
        yposbig = sparkcourse['true_ypos'].values
        xybig = np.array([xposbig, yposbig])


        xybig = xybig.transpose()

        xybig = xybig.reshape(-1, 1, 2)

#        segments = np.hstack([xybig[:-1], xybig[1:]])
#        coll = LineCollection(segments, linewidth=0.5, cmap=plt.cm.gist_rainbow, alpha = 0.5)
#        coll.set_array(np.array(range(len(df2))))
#
#
#        ax.add_collection(coll)


        x = sparkcourse_ds['true_xpos_ds']
        y = sparkcourse_ds['true_ypos_ds']
        z = sparkcourse_ds['mean_err']

        circles = [plt.Circle((xi,yi), radius=zi*12.5, linewidth=0, color = 'g', alpha = 0.2) for xi,yi,zi in zip(x,y,z)]
        circles2 = [plt.Circle((xi,yi), radius=zi*25, linewidth=0, color = 'g', alpha = 0.2) for xi,yi,zi in zip(x,y,z)]



        c2 = PatchCollection(circles2, color = 'c', linewidth=0, alpha = 0.1)
        c = PatchCollection(circles, color = 'w', linewidth=0, alpha = 0.1)
        ax.add_collection(c2)
        ax.add_collection(c)

        ax.scatter(x, y, marker = '+', s = 12.5, c = 'c')


    df['truex'] = truex

    df['truey'] = truey

    df['truex_ds'] = truex_ds

    df['truey_ds'] = truey_ds

    df2['displacement'] = displacement

    if outdir:
        fig.savefig(outdir + 'render\\' + imgname + '.png', dpi = my_dpi )

    else:
        fig.savefig(dir + 'render\\' + imgname + '.png', dpi = my_dpi )
    ax.set_aspect('equal', 'box')

    return df, df2, correctedx, correctedy


def create_circular_mask(h, w):

    center = (int(w/2), int(h/2))
    radius = min(center[0], center[1], w-center[0], h-center[1])

    Y, X = np.ogrid[:h, :w]
    dist_from_center = np.sqrt((X - center[0])**2 + (Y-center[1])**2)

    mask = dist_from_center <= radius
    return mask


def SC_metric(df, mask, rad = [0, 5, 10, 20, 50]):

    pointx = df['truex_ds'].values
    pointy = df['truey_ds'].values
    points = np.array([pointx, pointy]).T

    label_ypos, label_xpos = np.where(mask>0)
    coordinates = np.array([label_xpos, label_ypos]).T

    dists, index, coordinates = nearest(points, mask)

    for i in rad:
        if i==0:
            sc_l, sc_n = nd.label(mask)
            size = []
            cluster = []
            plt.imshow(sc_l)
            for j in index:
                ref = sc_l[coordinates[j, 1], coordinates[j, 0]]
                cluster.append(ref)
                size.append(np.sum(sc_l==ref))
            df['sc_0'] = np.array(size)
            df['sc_0_clref']=np.array(cluster)

        else:
            h = i*2+1
            kernel = create_circular_mask(h, h)
            dil = nd.morphology.binary_dilation(mask, kernel)
            sc_l, sc_n =  nd.label(dil)
            sc_l = sc_l*mask
            plt.imshow(sc_l)
            size = []
            cluster = []
            for j in index:
                ref = sc_l[coordinates[j, 1], coordinates[j, 0]]
                cluster.append(ref)
                size.append(np.sum(sc_l==ref))

            df ['sc_{}'.format(i)] = np.array(size)
            df['sc_{}_clref'.format(i)]=np.array(cluster)

    return df


def main_seq(df, df2, dir, img, events, xcorr, ycorr, imgname = 'fitted'):

    mask = generate_mask_local(img, area = 101, offset = -0.0001)
    calculate_fit_vol(df)

    df, df2, correctedx, correctedy = create_map(df, df2, img, imgname, xcorr, ycorr, outdir = dir, suppress_renders = True)

    if len(df) == 0:
        print ('no spark meets criteria')
        return


    dist, cluster_ref, label, sizes, mean_dens, pts, pixels, local_density = find_neighbours(df, mask, img, events)
    df = SC_metric(df, mask)

    try:
        if len(cluster_ref) == 0:
            print ('no spark meets criteria')

        sizedf = pd.DataFrame()
        sizedf['sizes'] = sizes/36
        df = in_df(df, dist, cluster_ref, pixels)
        df['local_density'] = local_density
        io.imsave(dir +'mask\\' + imgname + 'mask.tif', (label > 0).astype(np.int32))
        df.to_excel(dir+ 'raw\\' + imgname + "raws_processed2.xls")
        df2.to_excel(dir+ 'sum\\' + imgname + "sum_processed2.xls")
        sizedf.to_excel(dir+ 'size\\' + imgname + "ryr_sizes.xls")
        return

    except:
        print('issue in try loop')
        return


if __name__ == '__main__':

    dir = 'test\\data\\'
    dirpoint = 'test\\points\\'
    dir2 = 'test\\correlation\\'
    #

    for filename in os.listdir(dir):

        if filename.endswith(".tif"):
            img = io.imread(dir + filename)

            fname = filename.split('_ryr.')[0]
            #mask = io.imread(dir + "C4thresh.tif")
            #
            df = pd.read_excel(dir + fname + 'raws_edited.xls')
            dfpt =  pd.read_csv(dirpoint + fname + '_ryr.txt', sep='\t')
            ###remove from below for non cropped image
            dfpt['x'] = dfpt['x']-7475.0
            dfpt['y'] = dfpt['y']-13065.0
            dfpt = dfpt[dfpt['x']>0]
            dfpt = dfpt[dfpt['x']<5980]
            dfpt = dfpt[dfpt['y']>0]
            dfpt = dfpt[dfpt['y']<5980]
            #adjustments made as an ROI was taken for the test image.
            ###remove to here for non cropped image
            """
            points = np.vstack(dfpt['x'].values, dfpt['y'].values)

            dfpt['x'].values is the actual y values
            """

            points = np.vstack([dfpt['y'].values, dfpt['x'].values]).T
            #
            df2 = pd.read_excel(dir + fname + 'sum.xls')
            df2['spark x'] = df2['spark x'] + 2
            df2['spark y'] = df2['spark y'] + 2

            imgname = fname

            main_seq(df, df2, dir2, img, points, 0, 0, imgname = imgname)
            plt.close()
