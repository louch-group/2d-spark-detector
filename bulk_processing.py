# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 09:35:41 2019

short script for batch processing diffusion subtraction

@author: yufengh
"""

import os
import numpy as np
from skimage import io
import diffusion_calculation as diff


def folder_loop2(dir_in, dir_out):
    for filename in os.listdir(dir_in):
        if filename.endswith(".tif"):
            filename = filename.split('.')[0]
            print('processing image {}'.format(filename))
            img = io.imread(dir_in + filename + '.tif')
            out = diff.t1_subtraction (img, tl = 1, D = 1000, t = 0.002, offset = 0)
            io.imsave(dir_out + filename+'clean.tif', out.astype(np.float32))

    return


dir_in = "test//raw//"
dir_out = "test//clean//"

folder_loop2(dir_in, dir_out)
