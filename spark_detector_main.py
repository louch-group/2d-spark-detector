# -*- coding: utf-8 -*-
"""
Created on Tue Oct 23 15:05:23 2018
main file for sparkdetector

Update 29-1-2019: Added CLEAN decon function
Update 11-2-2019: Upgraded for Python 3.7 function
Update 11-5-2020: cleaned up dir, renamed imports, placed dir data to main,
                  Removed CLEAN decon function
Update 12-5-2020: Initial Git commit, rearranged folder strcuture
Update 13-10-2020: Set up main test case
Update 26-02-2020: Add in new comments explaining the functions and use cases


TODO: set up arg parser
TODO: integrate bg creation and diffusion subtraction elements

@author: yufengh
"""
import pandas as pd
from pandas import DataFrame
import numpy as np
import scipy.ndimage as nd
from skimage import io
from skimage import morphology
import matplotlib.pyplot as plt
import scipy.ndimage.morphology as morph
import os
from os import path
from Modules import Fitter
import multiprocessing as mp
import argparse

def folder_loop2(directory, output_dir, maskdir = None, directory2 = None, step = 1.0, scale =[10,1,1]):
    
    """
    main loop for batch processing images (tiffs only)
    please note specific requirement for file names:
        raws - whatever name (e.g. test.tif)
        clean image - filename + clean.tif (e.g. testclean.tif)
        mask - filename + _mask.tif (e.g. test_mask.tif)
        
    
    directory - location of raw Ca files for analysis
    output_dir - location for extracted spark and numerics to be saved to
    maskdir - location for mask images
    directory2 - location of clean files for analysis
    step - step size for iterative peak finding (other variables need to be 
                                                 changed in the fitter file)
    scale - scaling along the axis used for rough detection 
            (avoid changing [1] or [2] as this is yet to be implemented and tested.)
    
    """
    
    for filename in os.listdir(directory):

        if filename.endswith(".tif"):
            filename = filename.split('.')[0]
            mask = None
            print ('processing image {}'.format(filename))

            img = io.imread(directory + filename + '.tif')
            #img = nd.filters.gaussian_filter(img, (0,1,1))

            try:
                img2 = io.imread(directory2 + filename + 'clean.tif')
                print('resizing diffusion images')
                img2 = np.pad(img2, ((2,0),(0,0),(0,0)), mode = 'reflect')
                #img2 = nd.filters.gaussian_filter(img2, (0,1,1))

            except:
                img2 = None
                print('clean not found')

            try:
                mask = io.imread(maskdir + filename + '_mask.tif')

            except:
                mask = None
                print('mask not found')
                
            print ('beginning main analysis of {}'.format(filename))
            print ('image sizes:')
            print ('inp:{}, {}, {}'.format(img.shape[0], img.shape[1], img.shape[2]))
            if np.any(img2):
                print ('diff minus:{}, {}, {}'.format(img2.shape[0], img2.shape[1], img2.shape[2]))


            Fitter.main(img, scale, step, output_dir, filename, img2 = img2, mask = mask)
#            except:
#                print ('did not find mask, beginning main analysis of {}'.format(filename))
#                Fitter.main(img, scale, step, output_dir, filename, img2 = img2)
            continue
        else:
            continue
    return


def single_loop(filename, directory, output_dir, maskdir = None, directory2 = None, step = 2.0, scale =[10,1,1]):
    
    """
    single file processing
    
    please note, not well tested yet
    filename - image filename (e.g.test.tif) do not include entry tree
    rest is same as folder loop function
    """

    filename = filename.split('.')[0]
    mask = None
    print ('processing image {}'.format(filename))

    img = io.imread(directory + filename + '.tif')
    print('resizing raw image')
    #img = nd.filters.gaussian_filter(img, (0,1,1))

    try:
        img2 = io.imread(directory2 + filename + 'clean.tif')
        print('resizing diffusion images')
        img2 = np.pad(img2, ((2,0),(0,0),(0,0)), mode = 'reflect')
        #padding needed to match ds and inp img sizes
        #img2 = nd.filters.gaussian_filter(img2, (0,1,1))

    except:
        img2 = io.imread(directory2 + filename + 'clean.tif')


    try:
        mask = io.imread(maskdir + filename + '_mask.tif')

    except:
        mask = None
        print('mask not found')
    print ('beginning main analysis of {}'.format(filename))
    print ('image sizes:')
    print ('inp:{}, {}, {}'.format(img.shape[0], img.shape[1], img.shape[2]))
    print ('diff minus:{}, {}, {}'.format(img2.shape[0], img2.shape[1], img2.shape[2]))


    Fitter.main(img, scale, step, output_dir, filename, img2 = img2, mask = mask)

    return


if __name__=='__main__':
    
    
    #argparse.ArgumentParser(description='Calcium spark detection for 2d sparks')
    
    """
    main seq
    manual set directory for operation
    recommended to keep multiprocessing as False for now as it's not extensively tested yet'
    """
    
    use_multi = False
    
    
    directory = "test//raw//"
    dir2 = "test//clean//"
    maskdir = "test//mask//"
    outdir = "test//out//"

    if use_multi:
        cores = 6
        pool = mp.Pool(processes=cores)

        for filename in os.listdir(dir):

            if filename.endswith(".tif"):
                pool.apply_async(single_loop, args=(filename, directory, maskdir, outdir, dir2))

    else:
        folder_loop2(directory, outdir, maskdir = None, directory2 = dir2)
