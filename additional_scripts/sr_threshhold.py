# -*- coding: utf-8 -*-
"""
Created on Thu Oct 15 09:58:05 2020

@author: yufengh
"""

import numpy as np
from skimage import filters
from skimage import io
import os

def generate_mask_local(img, area = 101, offset = -0.00005):
    mask = (img > filters.threshold_local(img, area, offset = offset)).astype(np.int32)
    return mask

if __name__ == '__main__':
    dir = 'F:\\Correlation_paper\\sr\\ryr\\render\\'
    outdir = 'F:\\Dstorm Surface\\render\\output_PALM\\'
    
    for fname in os.listdir(dir):
        if fname.endswith('tif'):
            img = io.imread(dir + fname)
            thresheld = generate_mask_local(img, area = 101, offset = -0.0001)
            
            io.imsave(outdir + fname.split('.')[0] + '_mask.tif', thresheld.astype(np.float32))
            
            
    