# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 18:30:00 2019

utilities file for the main spark class

contains the main function for iterative fitting of frames

@author: yufengh
"""

import numpy as np
from skimage import io
import scipy.ndimage as nd

try:
    from Modules import GaussfitsClass as GFC
except:
    import GaussfitsClass as GFC
    
from tqdm import tqdm
import cProfile
import pstats

def stack_iteration(img, centroid, blurrad = 1, ref = '', roirad = 20, thresh = 2.0):

    blurimg = nd.filters.gaussian_filter(img, (0,blurrad,blurrad))
    spark_ref = np.repeat(ref, len(img))
    stack_params = np.empty((len(img),6))
    stackSNR = np.empty(len(img))
    residuals = np.empty(len(img))
    rsq = np.empty(len(img))
    raw_vol = np.empty(len(img))
    fit_vol = np.empty(len(img))
    errx = np.empty(len(img))
    erry = np.empty(len(img))
    Gclass = np.empty(len(img), dtype = object)

    checkpoint = []
    exp_bg = np.median(img[0:5])
    print (exp_bg)
    exp_bg = np.zeros(len(img)) + exp_bg

    for i in tqdm(range(len(img))):

#        if (i+1)%10==0:
#            print ("""Slice {0} of {1}""".format(i+1, total))
        rawslice = img[i]
        blurslice = blurimg[i]
        fitslice = GFC.gaussfitter(rawslice, blurimg = blurslice, thresh = thresh, median = exp_bg[i])
        Gclass[i] = fitslice

        Params = (fitslice.final_params.x[0],
                  fitslice.init_params.x[1],
                  fitslice.init_params.x[2],
                  fitslice.final_params.x[1],
                  fitslice.final_params.x[2],
                  fitslice.final_params.x[3])

        SNR = fitslice.SNR
        Mean_Residuals = fitslice.Residuals

        Rvol = fitslice.volume[0]
        Fvol = fitslice.volume[1]

        rsq[i] = fitslice.fit_rsq()
        checkpoint.append(fitslice.checkpoint)
        errx[i] = fitslice.accuracy[0]
        erry[i] = fitslice.accuracy[1]
        stack_params[i] = Params
        stack_params[i,2] = stack_params[i,2] - roirad
        stack_params[i,1] = stack_params[i,1] - roirad
        raw_vol[i] = Rvol
        fit_vol[i] = Fvol
        stackSNR[i] = SNR
        residuals[i] = Mean_Residuals


    checkpoint = np.array(checkpoint)


    output = {'spark_ref':spark_ref,
                'amplitude':stack_params[:,0],
                'x':stack_params[:,1],
                'y':stack_params[:,2],
                'width_x':stack_params[:,3],
                'width_y':stack_params[:,4],
                'rotation':stack_params[:,5],
                'SNR':stackSNR,
                'residuals':residuals,
                'raw_volume':raw_vol,
                'fit_volume':raw_vol,
                'r_squared':rsq,
                'checkpoint':checkpoint,
                'errx':errx,
                'erry':erry}


    return output, Gclass

if __name__ == '__main__':
    dir = 'D:\\Spark Detecter V2 Py37 (build 190726)\\testdata\\'
    
    img = io.imread(dir + 'Test1.tif')-32768
    
    profile = cProfile.Profile()
    profile.runcall(stack_iteration, img, [0,0,0])
    ps = pstats.Stats(profile)
    ps.print_stats()