# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 16:25:40 2019

A base class object for the fotting of 2d gaussians on specific images. 
Also incorperated is thresholds to indicate if a good fit has been achieved.

Update 29-1-2019: Added CLEAN decon function
Update 11-2-2019: Upgraded for Python 3.7 function

@author: yufengh
"""
import numpy as np
import scipy.optimize as opt
from skimage import io
import matplotlib.pyplot as plt
#from scipy import optimize,stats,pi


"""a class for gaussian fitting individual frames"""
class gaussfitter:

    def __init__(self, image, blurimg = None, median = 1000, thresh = 2, run_checkpoint = True):
        self.imgblur =  blurimg
        self.thresh = thresh 
        self.checkpoint = True
        self.image = image
        self.median = median
        
        if np.any(blurimg is None):
            self.imgblur =  self.image
            
        
        #-------------------------------------------------------------    
        """
        the gaussian parameters of a 2D distribution found by a fit
        
        this is to fit the blurred and unblurred (but constrained)
        
        (height, x,y,wx,wy,rot)
        """
            
        errorfunction1 = lambda p: self.error_fun(p[0], p[1], p[2], p[3], p[4], p[5], self.imgblur)
        lowbounds = (0,4,4, 4, 4, -np.inf)
        highbounds = (np.inf,len(self.image[0])-4,len(self.image)-4, (len(self.image[0])//2), (len(self.image)//2), np.inf)
        params = self.moments(self.imgblur, lowbounds, highbounds)
        
        self.init_params= self.fitgaussian(errorfunction1, lowbounds, highbounds, params)
        
        lowbounds_final = (0, 4, 4, -np.inf)
        highbounds_final = (np.inf, (len(self.image[0])//2), (len(self.image)//2), np.inf)
        
        #constraint p to not fit position
        errorfunction2 = lambda q: self.error_fun(q[0], self.init_params.x[1], self.init_params.x[2], q[1], q[2], q[3], self.image)
        
        paramsfinal = self.moments(self.image, 
                                   lowbounds, 
                                   highbounds, 
                                   set_bounds = True)
        paramsfinal = (paramsfinal[0],
                       paramsfinal[3],
                       paramsfinal[4],
                       paramsfinal[5])
        try:
            self.final_params = self.fitgaussian(errorfunction2, lowbounds_final, highbounds_final, paramsfinal)
            
        except:
            print ('could not do second pass using first pass instead')
            self.final_params = self.init_params
        #-------------------------------------------------------------
        
        fit = self.gaussian(self.final_params.x[0],
                            self.init_params.x[1],
                            self.init_params.x[2],
                            self.final_params.x[1], 
                            self.final_params.x[2],
                            self.final_params.x[3])

        
        fit = fit(*np.indices(self.image.shape))
        noise = self.image - fit
        noise2 = self.imgblur - fit
        self.Residuals = np.sum(np.abs(self.image))/self.image.size
        self.var = np.std(noise)
        self.SNR = self.final_params.x[0]/np.std(noise2)
        
        
        self.volume = self.vol_gaussian()
        
        self.accuracy = self.estimated_accuracy()
        self.set_checkpoints()
        #false checkpoints indicate a termination criteria is met and should not be considered in analysis
        
    def set_checkpoints(self):
            
        if self.final_params.x[1] >= len(self.image[0])//2:
            self.checkpoint = False
        elif self.final_params.x[2] >= len(self.image)//2:
            self.checkpoint = False
        elif self.final_params.success != True:
            print('fit failed')
            self.checkpoint = False
        elif self.SNR < self.thresh:
            self.checkpoint = False
#        elif self.fit_rsq() > self.rsq_thresh:
#            self.checkpoint = False
        elif np.mean(self.final_params.x[1:2]) > 15:
            self.checkpoint = False
        
        
    ##-----------------------------------------------------------------------------
    def moments(self, image, lowbounds = None, highbounds = None, set_bounds = True):
        """Returns (height, x, y, width_x, width_y)
        the gaussian parameters of a 2D distribution by calculating its
        moments """
        
        total = image.sum()
        if total == 0:
            return lowbounds[0], lowbounds[1], lowbounds[2], lowbounds[3], lowbounds[4], 0
        X, Y = np.indices(self.image.shape)
        x = (X*image).sum()/total
        y = (Y*image).sum()/total
        col = np.sum(image, axis = 0)
        width_x = np.sqrt(abs((np.arange(col.size)-y)**2*col).sum()/col.sum())
        row = np.sum(image, axis = 1)
        width_y = np.sqrt(abs((np.arange(row.size)-x)**2*row).sum()/row.sum())
        height = image.max()
        displacement = np.median(self.image)
        height = image.max() - displacement
        
        #check for whether estimate is within bounds set to bound if not
        if set_bounds == True:
            height = min(max(lowbounds[0], height), highbounds[0])
            x = min(max(lowbounds[1], x), highbounds[1])
            y = min(max(lowbounds[2], y), highbounds[2])
            width_x = min(max(lowbounds[3], width_x), highbounds[3])
            width_y = min(max(lowbounds[4], width_y), highbounds[4])
        
        return height, x, y, width_x, width_y, 0.0

    ##-----------------------------------------------------------------------------
    """gaussian general function"""
    def gaussian(self, height, center_x, center_y, width_x, width_y, rotation):
        """Returns a gaussian function with the given parameters"""

        displacement = self.median
        width_x = float(width_x)
        width_y = float(width_y)

        rotation = np.deg2rad(rotation)
        a = np.cos(rotation)**2/(2*width_x**2) + np.sin(rotation)**2/(2*width_y**2)
        b = -np.sin(2*rotation)/(4*width_x**2) + np.sin(2*rotation)/(4*width_y**2)
        c = np.sin(rotation)**2/(2*width_x**2) + np.cos(rotation)**2/(2*width_y**2)

        def rotgauss(x,y):
            """follows gaussian general formula detailed in wikipedia"""
            g = displacement + height*np.exp(
                -(a*(x-center_x)**2
                + 2*b*(x-center_x)*(y-center_y)
                + c*(y-center_y)**2))
            return g
        return rotgauss

    ##-----------------------------------------------------------------------------
    def error_fun(self, height, center_x, center_y, width_x, width_y, rotation, img):
        res = np.ravel(self.gaussian(height, center_x, center_y, width_x, width_y, rotation)(*np.indices(img.shape)) - img)
        res = np.nan_to_num(res)
        return res
    
    ##-----------------------------------------------------------------------------
    def fitgaussian(self, errorfunction, lowbounds, highbounds, params):
        """the gaussian parameters of a 2D distribution found by a fit"""
        
        """(height, x,y,wx,wy,rot)"""
        
        
        #try:
        final_params = opt.least_squares(errorfunction, 
                                       params, 
                                       bounds = (lowbounds, highbounds),
                                       loss = 'soft_l1',
                                       f_scale = 0.1,
                                       gtol = 1e-8)
        #except:
            #final_params = opt.least_squares(errorfunction, params)
            #self.checkpoint = False
        

        return final_params
    ##-----------------------------------------------------------------------------
    
    """
    function for deciding the fit error
    """
    
    def estimated_accuracy(self):
        
        fitsigx = self.final_params.x[1]
        fitsigy = self.final_params.x[2]
        sigxy = np.array([fitsigx, fitsigy]) 
        
        nphotons = self.volume[1]
        bg = self.var
        
        if nphotons <= 0:
            return [float('NaN'), float('NaN')]
        
        else:
            e1 = sigxy**2/nphotons
            e2 = (1/12)/nphotons
            e3 = (4 * np.sqrt(np.pi)*sigxy**3*bg**2)/(nphotons**2)
            
            res = np.sqrt(e1+e2+e3)
            return res

    ##-----------------------------------------------------------------------------
    """integral under the curve"""
    def vol_gaussian(self):
        fit = self.gaussian(self.final_params.x[0],
                            self.init_params.x[1],
                            self.init_params.x[2],
                            self.final_params.x[1], 
                            self.final_params.x[2],
                            self.final_params.x[3])
        fitted_curve = (fit(*np.indices(self.image.shape))-self.median)
        raw = self.image.astype(int) - self.median
        
        raw_sum = np.sum(raw)
        fitted_sum = np.sum(fitted_curve)
        return raw_sum, fitted_sum, raw, fitted_curve
    
    ##-----------------------------------------------------------------------------
    """plotting the resultant fit, can be called if unsure of the fit"""
    def plot_fit(self, Contour_map = plt.cm.copper, img_map = plt.cm.inferno, vmin = None, vmax = None):

        plt.matshow(self.image, cmap=img_map,  vmin =  vmin, vmax = vmax)
        var = self.final_params.x
        print (var)
        
        fit = self.gaussian(self.final_params.x[0],
                            self.init_params.x[1],
                            self.init_params.x[2],
                            self.final_params.x[1], 
                            self.final_params.x[2],
                            self.final_params.x[3])
        
        plt.contour(fit(*np.indices(self.image.shape)), cmap=Contour_map)
        ax = plt.gca()
        (height, x, y, width_x, width_y, rotation) = (self.final_params.x[0],
                                                        self.init_params.x[1],
                                                        self.init_params.x[2],
                                                        self.final_params.x[1], 
                                                        self.final_params.x[2],
                                                        self.final_params.x[3])
        
        rotation = rotation%360
        plt.text(0.95, 0.05, """
        x : %.1f
        y : %.1f
        width_x : %.1f
        width_y : %.1f
        Height : %.1f
        Rotation : %.1f
        SNR : %.1f""" %(x, y, width_x, width_y, height, rotation, self.SNR),
        fontsize=16, horizontalalignment='right',
        verticalalignment='bottom', transform=ax.transAxes)
        plt.show()

    ##-----------------------------------------------------------------------------
    """method for identifying the goodness of fit for the peak 2 standard deviations"""
    def fit_rsq(self, thresh = 2.0):
        var = self.final_params.x
        fit = self.gaussian(self.final_params.x[0],
                            self.init_params.x[1],
                            self.init_params.x[2],
                            self.final_params.x[1], 
                            self.final_params.x[2],
                            self.final_params.x[3])
        
        mask = var[0]
        threshval = (mask * 1/np.exp((thresh**2)/2)) + self.median
        mask = (self.image < threshval)
        
        fit_array = fit(*np.indices(self.image.shape))
        
        

        masked_array = np.ma.array(self.image, mask = mask)
        res_sq = (self.image - fit_array)**2
        masked_res_sq = np.ma.array(res_sq, mask = mask)
        
        img_mean = np.mean(self.image)
        fitmask = np.ma.array(((self.image - img_mean)**2), mask = mask)
        
        fit_res_sq = np.ma.sum(masked_res_sq)
        fit_mean_sq = np.ma.sum(fitmask)
        rsq = 1 - (fit_res_sq/fit_mean_sq)
        return rsq
    
        
"""============================================================================"""

    
    
        

