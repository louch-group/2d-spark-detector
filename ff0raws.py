# -*- coding: utf-8 -*-
"""
Created on Fri Aug  2 14:04:26 2019

@author: yufengh
"""

import pandas as pd
from skimage import io
import numpy as np
from tqdm import tqdm
import os

dir = 'test\\data\\sum\\'
dir2 = 'test\\data\\raws\\raws\\'
dir3 = 'test\\data\\raws\\'


def rawffo(dfsum, dfraw, imgname):

    ff0 = []
    ff0bg = []
    ff0ds = []
    ff0vol = []
    ff0volds = []

    for index, row in dfsum.iterrows():
        dftemp = dfraw[dfraw['sparkref'] == imgname + '{}'.format(index)]
        tempff0 = dftemp['amplitude'].values/row['bg']
        tempff0_ds = dftemp['amplitude_ds'].values/row['bg']
        tempff0vol = dftemp['rawvol'].values/row['bg']
        tempff0vol_ds = dftemp['rawvol_ds'].values/row['bg']
        tempff0bg = np.ones(dftemp['rawvol_ds'].values.shape)*row['bg']
        ff0.append(tempff0)
        ff0ds.append(tempff0_ds)
        ff0bg.append(tempff0bg)
        ff0vol.append(tempff0vol)
        ff0volds.append(tempff0vol_ds)

    ff0 = np.concatenate( ff0, axis=0 )
    ff0ds = np.concatenate( ff0ds, axis=0 )
    ff0vol = np.concatenate( ff0vol, axis=0 )
    ff0volds = np.concatenate( ff0volds, axis=0 )
    ff0bg = np.concatenate( ff0bg, axis=0 )

    dfraw['ff0'] = ff0
    dfraw['ff0_ds'] = ff0ds
    dfraw['bg'] = ff0bg
    dfraw['fvol'] = ff0vol
    dfraw['fvol_ds'] = ff0volds

    return dfraw


df = pd.DataFrame()
for filename in tqdm(os.listdir(dir)):
    if filename.endswith(".xls"):
        savefname = filename.split('edited')[0]
        dftemp = (pd.read_excel(dir+filename))
        dftemp['bg'] = dftemp['bg']
        dfraws = (pd.read_excel(dir2+savefname+'raws.xls'))
        dfraws = rawffo(dftemp, dfraws, savefname)

        dfraws.to_excel(dir3 + savefname + "raws_edited.xls")

#
