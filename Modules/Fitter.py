"""
Created untracked

fitting module that unifies the spark fitting pipeline

Update 29-1-2019: Added CLEAN decon function
Update 11-2-2019: Upgraded for Python 3.7 function
Update 12-5-2020: Initial git commit, moved to modules subdir

please note, file is imported by the main file and so co-dependencies in the 
modules folder are written as being imported from the upper directory for 
testing within this file please remove the "from modules" part of the imports

@author: yufengh
"""

import numpy as np
from skimage import io, morphology
from matplotlib.collections import LineCollection
import skimage.measure as ms
import matplotlib.pyplot as plt
import scipy.ndimage as nd
from Modules import SparkClass as sc
from Modules import GaussfitsClass as GFC
from Modules import resize
import pandas as pd
from skimage import io
import os
from tqdm import tqdm
from os import path

"""------------------------------------------------------------------
main run sequence:

main -> main_seq -> call fitting functions and outputs
------------------------------------------------------------------"""

def main(img, scale, step, outdir, fname, img2 = None, mask = None):
    
    ds = False

    if np.any(img2):
        ds = True

    if np.any(mask == None):
        mask = np.ones(img.shape)[0]

    img, imgdown, mask, img2 = prep(img, mask, scale[0], img2 = img2)
    print ('preparation complete')
    imgdown = imgdown
    
    io.imsave(outdir + 'testout.tif', imgdown)
    sparks = main_seq(img, imgdown, mask, scale, step, img2 = img2)

    #try:
    filtered_sparks = spark_filter(sparks)
    print ('Outputting sparks from {}'.format(fname))
    if len (filtered_sparks) == 0:
        print ("no spark detected")
    else:
        output_spark_parameters(filtered_sparks, outdir, fname, ds = ds)
        output_all_sparks(filtered_sparks, outdir, fname)
    #except TypeError:
    #    print ("no spark detected (or you messed up the print functions....)")

    return

def prep (img, mask, zscale, img2 = None):

    img = img[:,2:-2,2:-2]
    if np.any(img2):
        img2 = img2[:,2:-2,2:-2]
    img_downscaled = resize.resize(img, zscale)[1]
    #crop edges to remove edge artifacts (currently set to 2 pixels)
    img_downscaled = nd.filters.gaussian_filter(img_downscaled, (0,2,2))#gaussian to smooth out bg and sparks
    mask = (mask > 0).astype(int)
    mask = mask[2:-2,2:-2]
    return img, img_downscaled, mask, img2


def main_seq(imbig, imGauss, mask, scale, step, img2 = None, thresh = 10.0):

    threshimg = point_detect(imGauss, mask, step, base_thresh = thresh)
    raw_centroids = centroid_detect(threshimg)
    filtered_centroids = centroid_filter(raw_centroids, imGauss, mask)
    filtered_centroids = centroid_scale(filtered_centroids, scale)
    output = measure_sparks(imbig, mask, filtered_centroids, img2 = img2)
    return output

"""------------------------------------------------------------
Overhead methods (do not alter)
------------------------------------------------------------"""
def centroid_scale(centroid, factor):
    return centroid*factor
"""-----------------------------------------------------------------------"""
"""this is for making a disk mask for dilation step"""
def kernel (diam, height):
    discdiam = diam
    discheight  = height
    z,y,x = np.ogrid[-discdiam: discdiam+1, -discdiam: discdiam+1, -discheight: discheight+1]
    kernel = (x**2)/discdiam**2+(y**2)/discdiam**2+(z**2)/discheight**2 <= 1
    return kernel

"""-----------------------------------------------------------------------"""
"""basic thresholding with given levels"""
def threshfinding (img,bg, dev, start, stepsize = 1.0):

    threshimg = np.zeros(img.shape)
    threshimg[img > (bg + dev*(start*stepsize))] = 1

    return threshimg

"""-----------------------------------------------------------------------"""
"""medians with mask based ROI"""
def medians_calc(imGauss, mask):
    medians = np.empty(len(imGauss))


    for i in range(len(imGauss)):
        imGauss_copy = imGauss[i] * mask

        dist = imGauss_copy.flatten()
        dist = dist[dist > 0]#remove pixels out of mask
        medians[i] = np.median(dist)

    return medians
"""-----------------------------------------------------------------------"""
"""------------------------------------------------------------
Thresholding methods
------------------------------------------------------------"""

def stack_thresh(imGauss, mask, dev, iteration, step, minsize = 5, maxsize = 10000):

    img = np.empty(imGauss.shape)
    ker = kernel(10, 1)
    for i in range(len(imGauss)):
        imGauss[i] = imGauss[i] * mask

        dist = imGauss[i].flatten()

        dist = dist[mask.flatten() > 0]#remove pixels out of mask
        #don't need to sample every point saves time on finding medians
        halfgauss = np.sort(dist)[:(len(dist)//2)]
        left= halfgauss - np.max(halfgauss)
        right = np.abs(left)#flipping the axis for the other half
        combined = np.concatenate((left, right))#creating a complete distribution with only one tail of the normal for fitting
        bgvar = np.std(combined)#aka stdev
        #print (bgvar)
        #print (combined)
        #plt.hist(combined, bins = np.arange(-10, 10, 0.1))
        #calculating level of significance (set at 3 sigma currently)
        thresh = dev
        img[i] = threshfinding(imGauss[i], np.median(dist), bgvar, thresh, stepsize = step)

    ##    print i
    ##    print thresh

        img[i] = img[i] * mask#only keeping what's in the mask
        if (np.mean(dist)-2*bgvar) > np.median(dist):
            img[i] = img[i]*0

    #dilation morpho closing

    print ("brinary closing")
    img = morphology.binary_closing(img, selem=ker)
    ##img2 = img.astype(np.int32)
    ##print img2.shape
    ##io.imsave('D:\\NewCaData\\27-9-18\\Calcium\\FF0\\Out\\initial.tif', img2)
    print ("brinary closed")


    labeled, objno = nd.label(img)#assign unique label to each detected spark event
    sizes = nd.sum(img,labeled,np.arange(objno+1))[1:]

    #filter sparks for size, waves should be naturally excluded but this will exclude incase it's too big
    for i in range(objno):
        if sizes[i] < minsize:
            img[labeled == (i+1)] = 0
            continue
        elif sizes[i] > maxsize:
            img[labeled == (i+1)] = 0
            continue

    return img *(iteration+1)

"""------------------------------------------------------------
Iterative threshold peak detection
------------------------------------------------------------"""
#this block will run an interative stdev upscaling until only the peaks are identified.
#each iteration will use a higher number on the mask to indicate a higher search position
#then will filter for only the peaks to accurately identify spark centroids.
def point_detect(imgstk, mask, step, iteration = 5, base_thresh = 4):
    darray = [] #a list for storage of each iteration (yay 4d)
    for i in range(iteration):
        print ('iteration {}'.format(i))
        darray.append(stack_thresh(imgstk, mask, base_thresh+i, i, step, minsize = 15, maxsize = 10000))

    darray = np.array(darray)
    threshimg = darray.max(axis = 0)
    binary = threshimg > 0

    """label all regions in 3d"""

    labeled, objno = nd.label(binary)

    final = np.zeros(labeled.shape)#create a blank for inputting final mask


    """
    a bit complicated, but effectively stacking the labelled images together then
    finding the max value for each label region and only selecting those
    """
    print ("found {} objects".format(objno))
    print ("currently on object :")
    for i in tqdm(range(objno)):

        ##objno_image = (label == i).astype(int)
        roi = (labeled == i+1)
        intermediate = (threshimg*roi)*((threshimg*roi) == ((threshimg*roi).max()))
        ##zone = objno_image
        final[intermediate > 0] = 1
        ##print final.max()

    final = final.astype(int)
    #final is the resultant mask with detected centroids.
    return final


"""----------------------------------------------------------------
initial filter (pass over averaged image to optimise centroids)
------------------------------------------------------------------"""
#an initial filter on the detected spots to see if they meet gaussian fit criteria.
def centroid_filter(centroid, img, mask, rad = 2.0, thresh =5.0, roirad = 20, medians = 1000):

    try:
        medians = medians_calc(img, mask)

    except:
        pass
    tally = []
    tally1 = []


    for i in tqdm(range(len(centroid))):

        z = centroid[i,0]
        x = centroid[i,2]
        y = centroid[i,1]
        base = medians[z]
        #--------------------------------------------------------------------------------------
        #edge cropping (if exclusion rad set the same as crop rad, this will not be needed)
        #--------------------------------------------------------------------------------------
        if y-roirad < 0:
            if x-roirad < 0:
                centroidROI = img[z,0:(y+roirad+1),0:(x+roirad+1)]


            elif x+roirad+1 > len(img[0, 0]):
                centroidROI = img[z,0:(y+roirad+1),(x-roirad): len(img[0, 0])]


            else:
                centroidROI = img[z,0:(y+roirad+1),(x-roirad):(x+roirad+1)]





        elif y+roirad+1 > len(img[0]):
            if x-roirad < 0:
                centroidROI = img[z,(y-roirad):len(img[0]),0:(x+roirad+1)]


            elif x+roirad+1 > len(img[0, 0]):
                centroidROI = img[z,(y-roirad):len(img[0]),(x-roirad):len(img[0, 0])]


            else:
                centroidROI = img[z,(y-roirad):len(img[0]),(x-roirad):(x+roirad+1)]





        elif x-roirad < 0:
            centroidROI = img[z,(y-roirad):(y+roirad+1),0:(x+roirad+1)]


        elif x+roirad+1 > len(img[0, 0]):
            centroidROI = img[z,(y-roirad):(y+roirad+1),(x-roirad):len(img[0, 0])]


        else:
            centroidROI = img[z,(y-roirad):(y+roirad+1),(x-roirad):(x+roirad+1)]


        #--------------------------------------------------------------------------------------
        #edge cropping
        #--------------------------------------------------------------------------------------


        #try:

        spark_measured = GFC.gaussfitter(centroidROI, median = base)


        if spark_measured.SNR < thresh:
            tally.append(i)
            continue
        elif spark_measured.checkpoint == False:
            tally.append(i)
            tally1.append(i)
            continue

        #except:
            #print ('fitting error')
            #tally.append(i)

    print('removed {0} of {1} centroids due to not meeting fit criteria'.format(len(tally), len(centroid)))
    print('{} due to sub SNR detection'.format(len(tally)-len(tally1)))
    print('{} due to failed checkpoint'.format(len(tally1)))


    filtered_ROI = np.delete(centroid, tally, axis = 0)
    print ('final sparks: {}'.format(len(filtered_ROI)))


    return filtered_ROI


"""----------------------------------------------------------------
center detection
------------------------------------------------------------------"""
def centroid_detect(img):
    labeled, objno = nd.label(img)
    out = ms.regionprops(labeled)
    centroids = np.empty((objno, 3))

    for i in range(objno):
        centroids[i] = out[i].centroid

    tally = []


    #remove edge centroids
    for i in range(len(centroids)):
        if centroids[i,0] <= 5 or centroids[i,0] >= (len(labeled)-5):
            print ("object is too close to edge of frame")
            tally.append(i)
            continue
        elif centroids[i,1] <= 10 or centroids[i,1] >= (len(labeled[0])-10):
            print ("object is too close to edge of frame")
            tally.append(i)
            continue
        elif centroids[i,2] <= 10 or centroids[i,2] >= (len(labeled[0,0])-10):
            print ("object is too close to edge of frame")
            tally.append(i)
            continue

    centroidsCorrected = np.delete(centroids, tally, axis = 0)
    centroidsCorrected = (centroidsCorrected + [0,2,2]).astype(int)
    print (len(centroidsCorrected))

    print (tally)
    return centroidsCorrected

"""------------------------------------------------------------------
Method for spark measurements

img2 idicates the cleaned image if present
------------------------------------------------------------------"""
def measure_sparks(img, mask, true_centroid, img2 = None, roirad = 20, stkdepth = 100):


    height = stkdepth
    data = []

    for i in range(len(true_centroid)):
        zrangepos = int(0.3*height)
        zrangeneg = int(0.7*height)


        z = true_centroid[i,0]
        print (z)
        x = true_centroid[i,2]
        print (x)
        y = true_centroid[i,1]
        print (y)
        #--------------------------------------------------------------------------------------
        #edge cropping (if exclusion rad set the same as crop rad, this will not be needed)
        #--------------------------------------------------------------------------------------

        roiZ = [max(z-zrangepos, 0),min(z+zrangeneg, img.shape[0])]
        roiY = [max(y-roirad, 0),min(y+roirad+1, img.shape[1])]
        roiX = [max(x-roirad, 0),min(x+roirad+1, img.shape[2])]

        print ("measuring spark {0} of {1}".format(i+1, len(true_centroid)))

        centroidROI = img[roiZ[0]:roiZ[1],roiY[0]:roiY[1],roiX[0]:roiX[1]]
        base = np.median(centroidROI[:5])
        ds_centroidROI = None
        if np.any(img2):
            ds_centroidROI = img2[roiZ[0]:roiZ[1],roiY[0]:roiY[1],roiX[0]:roiX[1]]
        spark_measured = sc.Sparks(centroidROI, true_centroid[i], medians = base, stk2 = ds_centroidROI)
        data.append(spark_measured)




    data = np.array(data)
    #data = data.squeeze()
    print (data)
    return data


"""------------------------------------------------------------------
Additional methods for post processing and visualisation
------------------------------------------------------------------"""
def spark_filter (sparks):
    tally = []
    for i in range(len(sparks)):
        if sparks[i].trigger == False:
            tally.append(i)
            continue
    tally = np.array(tally)
    
    if len(tally) != 0:
        filtered = np.delete(sparks, tally)
    
    else:
        filtered = sparks
    return filtered

#--------------------------------------------------------------------------------------


def plot_spark (spark, img, scale):
    limits = spark.Spark_prune()
    cent = spark.centroid[1:]
    centroidbig = cent * scale

    xpos = (spark.parameters[:,1]*scale).astype(int)
    ypos = (spark.parameters[:,2]*scale).astype(int)
    xpos = xpos[limits[0]:limits[1]]
    ypos = ypos[limits[0]:limits[1]]

    xposbig = xpos+centroidbig[0]
    yposbig = ypos+centroidbig[1]

    xybig = np.array([yposbig,xposbig])
    xybig = xybig.transpose()
    xybig = xybig.reshape(-1, 1, 2)

    segments = np.hstack([xybig[:-1], xybig[1:]])
    coll = LineCollection(segments, cmap=plt.cm.gist_ncar)
    coll.set_array(np.array(range(len(xpos))))
    fig, ax = plt.subplots(1, 1)
    ax.matshow(img, cmap= plt.cm.inferno)
    ax.add_collection(coll)
    plt.show()
    return
#--------------------------------------------------------------------------------------
def plot_sparks_all(spark_set, img, scale):
    fig, ax = plt.subplots(1, 1)
    ax.matshow(img, cmap= plt.cm.inferno)

    for i in range (len(spark_set)):

        limits = spark_set[i].Spark_prune()

        if np.any(limits) == False:
            print ("Spark number %d is unplotable" %(i + 1))
            continue
        else:
            cent = spark_set[i].centroid[1:]
            centroidbig = cent * scale

            xpos = (spark_set[i].parameters['x']*scale).astype(int)
            ypos = (spark_set[i].parameters['y']*scale).astype(int)
            xpos = xpos[limits[0]:limits[1]]
            ypos = ypos[limits[0]:limits[1]]

            xposbig = xpos+centroidbig[0]
            yposbig = ypos+centroidbig[1]

            xybig = np.array([yposbig,xposbig])
            xybig = xybig.transpose()
            xybig = xybig.reshape(-1, 1, 2)

            segments = np.hstack([xybig[:-1], xybig[1:]])
            coll = LineCollection(segments, cmap=plt.cm.gist_ncar)
            coll.set_array(np.array(range(len(xpos))))


            ax.add_collection(coll)
            continue

    ax.set_aspect('equal', 'box')

    plt.show()
    return
#--------------------------------------------------------------------------------------
def output_spark_parameters(spark, dir, fname, scaling = False, scale = [1,1,1], ds = False ):
    data = []
    centroid = []
    df2 = pd.DataFrame()
    for i in range(len(spark)):
        params = np.array(spark[i].spark_characteristics())
        limits = spark[i].Spark_prune()
        ref = np.empty((1 + limits[1]-limits[0]), dtype = object)
        ref[:] = fname + '%d'%(i)
        """this is referencing parameters within the fit range"""

        temp_dict ={'amplitude':spark[i].parameters['amplitude'][limits[0]-1:limits[1]],
                    'xpos':spark[i].parameters['x'][limits[0]-1:limits[1]],
                    'ypos':spark[i].parameters['y'][limits[0]-1:limits[1]],
                    'xwidth':spark[i].parameters['width_x'][limits[0]-1:limits[1]],
                    'ywidth':spark[i].parameters['width_y'][limits[0]-1:limits[1]],
                    'xerr':spark[i].parameters['errx'][limits[0]-1:limits[1]],
                    'yerr':spark[i].parameters['erry'][limits[0]-1:limits[1]],
                    'rotation':spark[i].parameters['rotation'][limits[0]-1:limits[1]],
                    'rawvol':spark[i].parameters['raw_volume'][limits[0]-1:limits[1]],
                    'SNR':spark[i].parameters['SNR'][limits[0]-1:limits[1]],
                    'sparkref':ref}

        if ds:

            temp_dict['amplitude_ds'] = spark[i].ds_parameters['amplitude'][limits[0]-1:limits[1]]
            temp_dict['xpos_ds'] = spark[i].ds_parameters['x'][limits[0]-1:limits[1]]
            temp_dict['ypos_ds'] = spark[i].ds_parameters['y'][limits[0]-1:limits[1]]
            temp_dict['xwidth_ds'] = spark[i].ds_parameters['width_x'][limits[0]-1:limits[1]]
            temp_dict['ywidth_ds'] = spark[i].ds_parameters['width_y'][limits[0]-1:limits[1]]
            temp_dict['xerr_ds'] = spark[i].ds_parameters['errx'][limits[0]-1:limits[1]]
            temp_dict['yerr_ds'] = spark[i].ds_parameters['erry'][limits[0]-1:limits[1]]
            temp_dict['rawvol_ds'] = spark[i].ds_parameters['raw_volume'][limits[0]-1:limits[1]]
            temp_dict['checkpoint_ds'] = spark[i].ds_parameters['checkpoint'][limits[0]-1:limits[1]]
            temp_dict['SNR_ds'] = spark[i].ds_parameters['SNR'][limits[0]-1:limits[1]]


        tempdf = pd.DataFrame.from_dict(temp_dict)
        df2 = df2.append(tempdf)
        data.append(params)
        centroid.append(spark[i].centroid)

    data = np.array(data)
    centroid = np.array(centroid)
    """convert index to referrable object"""
    a = np.array(df2.index)
    df2['indref'] = a
    """------------------------------------------------------------------"""
    if scaling == True:
        data[:,1] = data[:,1] * scale[0]
        data[:,2] = data[:,2] * scale[0]
        centroid = centroid * scale

    datadict = {'amplitude':data[:,0],
                'halfmax':data[:,1],
                'time peak':data[:,2],
                'sparkmass':data[:,3],
                'spark z':centroid[:,0],
                'spark y':centroid[:,1],
                'spark x':centroid[:,2]}


    df = pd.DataFrame.from_dict(datadict)
    df.to_excel(dir + fname +'.xls')


    df2.to_excel(dir + fname + 'raws.xls')

    print ('data saved to {0}'.format(dir))
    return


def output_all_sparks(data, dir, fname):
    print ("preparing to save spark stacks as tiff files")
    for i in range(len(data)):
        savename = fname + ("%d" %(i))
        data[i].save_spark_tif(savename, dir)

    return
