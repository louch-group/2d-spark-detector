# -*- coding: utf-8 -*-
"""
Created on Fri Aug  2 10:57:40 2019

@author: yufengh
"""

import pandas as pd
from skimage import io
import numpy as np
from tqdm import tqdm
import os


dir = 'test\\data\\sum\\raws\\'
dir2 = 'test\\bg\\'
dir3 = 'test\\data\\sum\\'


def measurebg(df, img, roi = 20, bgoffset = 100):

    x0 = 0
    x1 = img.shape[2]

    y0 = 0
    y1 = img.shape[1]

    z0 = 0
    z1 = img.shape[0]

    bg = []

    for index, row in df.iterrows():

        cx = row['spark x'].astype(int)
        cy = row['spark y'].astype(int)
        cz = row['spark z'].astype(int)
        cx0 = max((cx - roi), x0)
        cy0 = max((cy - roi), y0)
        cz0 = max((cz - roi), z0)

        cx1 = min((cx + roi), x1)
        cy1 = min((cy + roi), y1)
        cz1 = min((cz + roi), z1)

        imgroi = img[cz0:cz1,cy0:cy1,cx0:cx1]

        bgpercent = np.percentile(imgroi, 5)

        bg.append(bgpercent-bgoffset)

    df['bg'] = bg

    return df


df = pd.DataFrame()

for filename in tqdm(os.listdir(dir)):
    if filename.endswith(".xls"):
        dftemp = (pd.read_excel(dir+filename))

        bgname = filename.split('.')[0]
        savefname = filename.split('.')[0]

        bg = io.imread(dir2 + bgname + '_bg.tif')
        dftemp =  measurebg(dftemp, bg, roi = 20, bgoffset = 100)

        dftemp.to_excel(dir3 + savefname + "edited.xls")

#
