# 2D Spark Detector

Detector for 2d Calcium sparks in isolated CM prep also included is preprocessing and correlation scripts to match with super resolution images

## Requirements

- python 3.6+
- numpy
- scipy
- pandas
- xlwt
- scikit-learn
- matplotlib
- tqdm
- Python microscopy (used independently for rendering super resolution images)

## About the program

This program is used for detection of transient calcium release events known as calcium sparks, and tracking of its sites to a correlated release cluster of RyR2 protein. It is an essential component in the paper "Linking Calcium Sparks to Nanoscale Ryanodine Receptor Configuration in Cardiomyocytes" currently being submitted to publication

## Important notes
It is assumed that the acquired images are noisy in nature and so the script will not work with perfectly clean images. This is because it relies on estimating SNR for detection of peaks.

There is also many aspects that are hard coded for the images used in the corresponding paper (e.g. pixel sizes, expected snr, anchors for interpolation etc). A series of test data is provided to showcase the behaviour of the various components. There are plans to make the system more easily transferable between different imaging setups and conditions later.

## Flowchart for data pipeline
```mermaid
graph TD;
  classDef script fill:#bebfc2;
  A[Microscope Calcium Image] --> B[BG_interpolation.py]:::script
  B --> C[Extracted Background]
  B --> D[Background removed image]
  D --> E[bulk_processing.py]:::script
  E --> F[diffusion subtracted image]
  D --> G[spark_detector_main.py]:::script
  F --> G
  Abg[cell area mask] --> G
  G --> H[framewise data]
  G --> I[sparkwise data]
  H --> J[ff0raws.py]:::script
  I --> K[ff0.py]:::script
  C --> K
  K --> J
  J --> L[consolidated frame data]
  K --> M[consolidated summary data]
  N[RyR point data] --> P[Correlationv2.py]:::script
  O[RyR render] --> P
  L --> P
  M --> P
  P --> Q[Correlated data]
  Q --> R[Statistical Processing]

```
## Script specifics
This section details the specific scripts outlined in the pipeline

### BG_interpolation.py
Calculates the estimated background on a per pixel basis through spline interpolation through the z axis (time)

indir - directory for raw microscope image<br/>
outdir - directory for output

outputs - returns 2 images for estimated background and background subtracted calcium signal

### bulk_processing/diffusion_calculation.py
Calculates diffusion subtraction based off filtered calcium signal. bulk_processing refers to the batch method, whereas diffusion_calculation operates on individual images

dir - input calcium image directory<br/>
dir2 - output directory for diffusion subtracted data

### spark_detector_main.py
Main spark detection routine.

directory - directory for raw calcium input<br/>
dir2 - directory for diffusion subtracted calcium input<br/>
maskdir - directory for cell area mask input<br/>
outdir - directory for output

please note files in directory must be named according to the following convention:<br/>
raw image - '[filename].tif'<br/>
ds image - '[filename]clean.tif'<br/>
mask - '[filename]_mask.tif'<br/>

outputs each detected spark roi as small tiff files, additionally also a raw xls for the framewise detection parameter for each spark (all sparks merged to one file) and a summary xls for the individual spark parameters per image analysed.

### ff0.py, ff0raws.py
used for calculation of df/f0

uses the background image and spark localisation to provide calculations to measure df/f0 for each recorded spark in an image.

run ff0.py first<br/>
please note naming convention:<br/>
bg - '[filename]_bg.tif'

dir - directory for summary xls<br/>
dir2 - bg image <br/>
dir3 - output directory <br/>

then run ff0raws.py
dir - directory for summary (with ff0) xls<br/>
dir2 - directory for raw <br/>
dir3 - output directory <br/>

### Correlationv2.py
carries out correlation with RyR2 and filters spark data (outlined in main text of manuscript) many inputs and outputs with specific folder structure needed.

Please note in order to match point data to the roi selected in the test image there is a code block adjusting the point table.

dir - directory for raws, summary, and rendered RyR2 SR image<br/>
dirpoint - directory for points table from RyR SR<br/>
dir2 - directory for output (note needs specific sub directory structure see below)<br/>

#### naming convention for Correlationv2.py

inputs:<br/>
ryr2 sr image - [filename]_ryr.tif<br/>
frame data - [filename]raws_edited.xls<br/>
spark sumamry data - [filename]sum.xls<br/>
ryr2 point data - [filename]_ryr.txt<br/>

outputs:<br/>
output folder tree:<br/>

```mermaid
graph TD;
  A[dir] --> B[dir/mask]
  A --> C[dir/sum]
  A --> D[dir/raw]
  A --> E[dir/render]
  A --> F[dir/size]
```
