# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 18:23:14 2019

main class for each of the fitted spark

@author: yufengh
"""

import numpy as np
from skimage import io
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
import scipy.ndimage as nd

try:
    from Modules import spark_utils as su
except:
    import spark_utils as su
    
import copy
#from Modules import GaussfitsClass as GFC
import cProfile
import pstats



class Sparks:
    def __init__(self, imagestk, centroid, medians, stk2 = None, RSQthreshold = 0.4, SNRthreshold = 2.5, time = 1, ref = '', roirad = 20):

        """
        input variables
        """

        self.imgstk = imagestk
        self.centroid = centroid
        self.medians = medians
        self.stk2 = stk2
        self.trigger = True
        self.RSQthreshold = RSQthreshold
        self.slice_intervals = time #in ms
        self.SNRthreshold = SNRthreshold
        self.ref = ref

        """
        calculated variables (stored in parameters dict):

            stack_params,
            stackSNR,
            residuals,
            Raw_fits,
            Raw_vol,
            Fit_vol,
            rsq,
            checkpoint,
            Gclass

        fit roi may not be necessary, Gclass stores all the class data from the gaussian fits class
        """
    #def __call__(self):
        print ("fitting spark at location:")
        print ("""Z: {0:.1f} Y: {1:.1f} X: {2:.1f}""".format(self.centroid[0], self.centroid[1], self.centroid[2]))
        (self.parameters,
         self.Gclass) = su.stack_iteration(self.imgstk, self.centroid, ref = self.ref)

        """
        if diffusion subtraction present calculate the same paramters for stk
        """

        if self.stk2 is not None:
            (self.ds_parameters,
             self.ds_Gclass) = su.stack_iteration(self.stk2, self.centroid, ref = self.ref + 'diff_subtracted')

        self.set_trigger()



    def set_trigger(self):

        """
        filtering with checkpoint to ensure only those that fit within expected ranges are acceptd
        """

        vals = self.Spark_prune()

        label, objno = nd.label(self.parameters['checkpoint'])

        sizes = nd.sum(self.parameters['checkpoint'],label,np.arange(objno+1))


        if np.max(sizes) < 5 :
            print ("Spark is undetected within region size too small")
            self.trigger = False

        if vals[0] < 5:
            print ("Calcium release for ROI is unstable (initial frames shows positive detection)")
            self.trigger = False

        if vals[1] > (len(self.imgstk)-5):
            print ("Possible wave (final frames shows positive detection)")
            self.trigger = False

        try:
            if np.min(np.mean(np.array([self.parameters['width_x'],self.parameters['width_y']]), axis = 0)[vals[0]:vals[1]]) > 10.0:
                sparkwmin =  np.min(np.mean(np.array([self.parameters['width_x'],self.parameters['width_y']]), axis = 0)[vals[0]:vals[1]])
                print ('spark width: {}'.format(sparkwmin))
                print ('Spark is out of focus')
                self.trigger = False

            if self.parameters['SNR'][vals[0]:vals[1]].max() < self.SNRthreshold:
                print ('Peak SNR too low')
                self.trigger = False

        except:
            print ("error occured, please abort")
            self.trigger = False



    def Spark_prune(self, thresh = 2.5):
        
        """
        filtering for frames, remove those with calculated snr < threshold
        
        returns a range for spark start and spark finish
        """

        threshimg = self.parameters['SNR'] > thresh
#        threshimg2 = self.parameters['width_x'] < (self.imgstk.shape[2]//2 -1)
#        threshimg3 = self.parameters['width_y'] < (self.imgstk.shape[1]//2 -1)
#        threshimg = np.logical_and(threshimg, threshimg2)
#        threshimg = np.logical_and(threshimg, threshimg3)
        threshimg = nd.binary_closing(threshimg)

        label, objno = nd.label(threshimg)

        sizes = nd.sum(threshimg,label,np.arange(objno+1))
        i = np.where(sizes == sizes.max())

        i = i[0]

        if sizes.max() > (len(threshimg)*0.75):
            self.trigger = False

        if len (i) > 1:
            i = i[0]

        obj = np.where(label == i)
        val_range = np.array([obj[0][0],obj[0][-1]])


        return val_range



    def FWHM(self, limits):
        limit_params = self.parameters['amplitude'][limits[0]:limits[1]]
        half_max = (np.max(limit_params) - self.medians)/2
        FW = np.sum([limit_params>half_max])
        print ("FWHM: {} frames".format(FW))
        return FW

    def time_to_peak(self, limits):
        limit_params = self.parameters['amplitude'][limits[0]:limits[1]]
        time_of_peak = np.argmax(limit_params)
        print ("time to peak: {} frames".format(time_of_peak))
        return time_of_peak+1

    def spark_mass(self, limits):
        mass = np.sum(self.parameters['raw_volume'][limits[0]:limits[1]])
        print ("spark mass: {} (arb)".format(mass))
        return mass


    def spark_characteristics(self):
        """outputting the time to peak, FWHM, amplitude, total magnitude of spark."""
        limits = self.Spark_prune()
        limit_params = self.parameters['amplitude'][limits[0]:limits[1]]
        peak_amp = np.max(limit_params)
        print ("spark amp: {0:.4f}(arb)".format(peak_amp/1000.0))
        FW = self.FWHM(limits)
        ttp = self.time_to_peak(limits)
        sm = self.spark_mass(limits)
        return peak_amp, FW, ttp, sm

    def Plot_timecourse(self):
        x_axis = np.array(range(len(self.parameters['amplitude'])))*self.slice_intervals
        plt.plot(x_axis, self.parameters['amplitude'])
        plt.show()

    def save_spark_tif(self, fname, dir):
        io.imsave(dir + fname + ".tif", self.imgstk)
        try:
            io.imsave(dir + fname + "ds.tif", self.stk2)
        except:
            print('stk2 output failed')
        return

    def Plot_everything_pruned(self, plot_range = False, spacer = (0,0)):
        """
        pretty much useless with other functions made later, but can still have 
        some use if a bunch of random paramters in fitted spark is desired
        """
        
        limits = plot_range

        if plot_range == False:
            limits = self.Spark_prune()


        if np.any(limits):

            param = copy.deepcopy(self.parameters)

            for i in self.parameters.keys():
                param[i] = self.parameters[i][limits[0]-spacer[0]:limits[1]+spacer[1]]#minus and plus just to allow a bigger range

            """setup for coloured line for centroid timecourse"""
            x = param['x']
            y = param['y']
            xy = np.array([y,x])
            xy = xy.transpose()
            xy = xy.reshape(-1, 1, 2)
            segments = np.hstack([xy[:-1], xy[1:]])

            yw = np.mean(np.array([param['width_x'],param['width_y']]), axis = 0)
            xw = np.arange(len(x))*self.slice_intervals
            xh = np.arange(len(param['amplitude']))*self.slice_intervals
            yh = param['amplitude']
            xvol = np.arange(len(param['raw_volume']))*self.slice_intervals
            yvolf = param['fit_volume']
            yvolr = param['raw_volume']

            """setup plot area"""
            fig, ax = plt.subplots(1, 1)

            """plot 1, centroid movement"""

            coll = LineCollection(segments, cmap=plt.cm.gist_ncar)
            coll.set_array(np.array(range(len(x))))#must be ndarray
            ax.add_collection(coll)
            ax.autoscale_view()
            ax.set_title('centroid position')

            """plot 2, mean fit width"""

            plt.figure()
            plt.plot(xw, yw)
            plt.title('Mean width')

            """plot 3, amplitude"""
            fig2, ax2 = plt.subplots(1,1)
            ax2.plot(xh, yh)
            ax2.set_title('Amplitude, Vol')

            """plot 4, volume"""
            ##xsnr = np.arange(len(self.parameters[:,1]))*self.slice_intervals
            ##ax[1,0] = plt.plot(xsnr, self.SNR)
            ##ax[1,1].title('SNR')
            ax3 = ax2.twinx()
            ax3.plot(xvol, yvolf,  xvol, yvolr)
            plt.show()

        else:
            print ("Spark does not appear to fit required parameters (limits return = false)")


